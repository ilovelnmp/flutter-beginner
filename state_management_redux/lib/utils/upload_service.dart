import 'dart:io';

import '../utils/http_util.dart';

class UploadService {
  static const String uploadBaseUrl = 'http://localhost:3900/api/upload/';
  static Future uploadImage(String key, File file) async {
    var result = await HttpUtil.uploadSingle(
      uploadBaseUrl + 'image',
      key,
      file,
    );

    return result;
  }
}
