## MobX 状态管理 相关篇章源码

-   `counter`：简单的 MobX 计数示例

## 运行说明

-   对于存在网络请求的，请下载后端配套代码（基于 Express.js 和 MongoDB）：[后端配套代码](https://gitee.com/island-coder/express-api)
-   运行：`flutter pub get` 获取最新依赖插件
-   运行代码

## 相关文章

请到掘金社区查阅专栏文章：[Flutter 入门与实战](https://juejin.cn/column/6960631670378594311)

## 关注岛上码农

![岛上码农.jpg](https://gitee.com/island-coder/flutter-beginner/raw/master/qrcode_island_coder.jpg)
