import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:home_framework/models/dynamic_entity.dart';
import 'package:home_framework/utils/dialogs.dart';

import 'api/dynamic_service.dart';
import 'interfaces/dynamic_listener.dart';

class DynamicDetailPage extends StatefulWidget {
  final String id;
  DynamicDetailPage(this.id, {Key key}) : super(key: key);

  @override
  _DynamicDetailPageState createState() => _DynamicDetailPageState();
}

class _DynamicDetailPageState extends State<DynamicDetailPage> {
  DynamicEntity _dynamicEntity;

  @override
  void initState() {
    super.initState();
    _getDynamic(widget.id);
  }

  void _getDynamic(String id) async {
    EasyLoading.showInfo('加载中...', maskType: EasyLoadingMaskType.black);
    try {
      var response = await DynamicService.get(id);
      if (response.statusCode == 200) {
        setState(() {
          _dynamicEntity = DynamicEntity.fromJson(response.data);
        });
        _updateViewCount();
      } else {
        Dialogs.showInfo(this.context, response.statusMessage);
      }
    } on DioError catch (e) {
      Dialogs.showInfo(this.context, e.message);
    } catch (e) {
      Dialogs.showInfo(this.context, e.toString());
    }

    EasyLoading.dismiss();
  }

  void _updateViewCount() async {
    try {
      var response = await DynamicService.updateViewCount(_dynamicEntity.id);
      if (response.statusCode == 200) {
        setState(() {
          _dynamicEntity.viewCount = response.data['viewCount'];
          GetIt.instance.get<DynamicListener>().dynamicUpdated(
                _dynamicEntity.id,
                _dynamicEntity,
              );
        });
      }
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_dynamicEntity == null) {
      return Center(
        child: Text('加载中...'),
      );
    }
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text('动态详情'),
          brightness: Brightness.dark,
        ),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 10, 10, 5),
                child: Text(
                  _dynamicEntity.title,
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            _viewCountWrapper(_dynamicEntity.viewCount.toString()),
            SliverToBoxAdapter(
              child: CachedNetworkImage(
                imageUrl: _dynamicEntity.imageUrl,
                height: 200,
                fit: BoxFit.fill,
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                margin: EdgeInsets.all(10),
                child: Text(
                  _dynamicEntity.content,
                  style: TextStyle(
                    fontSize: 14.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      onWillPop: () async {
        //Navigator.of(context).pop({'id': widget.id});
        return true;
      },
    );
  }

  SliverToBoxAdapter _viewCountWrapper(String text) {
    return SliverToBoxAdapter(
      child: Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
        height: 20,
        child: Row(children: [
          Icon(
            Icons.remove_red_eye_outlined,
            size: 14.0,
            color: Colors.grey,
          ),
          SizedBox(width: 5),
          Text(
            text,
            style: TextStyle(color: Colors.grey, fontSize: 14.0),
          ),
        ]),
      ),
    );
  }
}
