import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'utils/http_util.dart';

class AppointmentPage extends StatefulWidget {
  AppointmentPage({Key key}) : super(key: key);

  @override
  _AppointmentPageState createState() => _AppointmentPageState();
}

class _AppointmentPageState extends State<AppointmentPage> {
  bool _hasBug = false;
  CancelToken _token;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_hasBug ? '该死的Bug!' : '约会中...',
            style: Theme.of(context).textTheme.headline4),
      ),
      body: Container(
        child: Center(
          child: Image.asset(_hasBug ? 'images/bug.png' : 'images/dating.png'),
        ),
      ),
      floatingActionButton: IconButton(
        icon: Icon(Icons.call),
        onPressed: () {
          _bugHappened();
        },
      ),
    );
  }

  void _bugHappened() {
    _token = CancelToken();
    HttpUtil.get('https://www.github.com', cancelToken: _token)
        .then((value) => {
              if (mounted)
                {
                  setState(() {
                    _hasBug = _token.isCancelled;
                  })
                }
            })
        .onError((error, stackTrace) => {});
    //_token.cancel('dispose');
  }

  @override
  void deactivate() {
    if (_token != null) {
      _token.cancel('dispose');
    }
    super.deactivate();
  }
}
