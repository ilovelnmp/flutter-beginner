import 'package:dio/dio.dart';

import '../utils/http_util.dart';

class JuejinService {
  static Future listArticles(String userId,
      {int sortType = 2, String cursor = '0', CancelToken cancelToken}) {
    return HttpUtil.post(
        'https://api.juejin.cn/content_api/v1/article/query_list',
        data: {'user_id': userId, 'sortType': sortType, 'cursor': cursor},
        cancelToken: cancelToken);
  }
}
