import 'package:flutter/material.dart';
import 'package:home_framework/mock/contactor_mock_data.dart';
import '../../models/contactor_entity.dart';

class ContactorListModel with ChangeNotifier {
  final List<ContactorEntity> _contactors = [];
  List<ContactorEntity> get contactors => _contactors;
  int _currentPage = 1;
  final int _pageSize = 20;

  void listContactors() async {
    List<Map<String, dynamic>> contatorsJson =
        await ContactorMockData.list(_currentPage, _pageSize);
    if (_currentPage == 1) {
      _contactors.clear();
    }
    contatorsJson.forEach((json) {
      _contactors.add(ContactorEntity.fromJson(json));
    });

    notifyListeners();
  }

  void refresh() {
    _currentPage = 1;
    listContactors();
  }

  void load() {
    _currentPage += 1;
    listContactors();
  }
}
