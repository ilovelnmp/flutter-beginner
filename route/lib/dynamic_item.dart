import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:home_framework/models/dynamic_entity.dart';
import 'package:home_framework/routers/fluro_router.dart';

class DynamicItem extends StatelessWidget {
  final DynamicEntity dynamicEntity;
  static const double ITEM_HEIGHT = 100;
  static const double TITLE_HEIGHT = 80;
  static const double MARGIN_SIZE = 10;
  const DynamicItem(this.dynamicEntity, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.all(MARGIN_SIZE),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _imageWrapper(this.dynamicEntity.imageUrl),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _titleWrapper(context, dynamicEntity.title),
                  _viewCountWrapper(dynamicEntity.viewCount.toString()),
                ],
              ),
            )
          ],
        ),
      ),
      onTap: () async {
        var arguments = await RouterManager.router.navigateTo(context,
            '${RouterManager.dynamicPath}/${dynamicEntity.id}?event=a&event=b');
        if (arguments != null) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("从动态${(arguments as Map<String, dynamic>)['id']}返回"),
          ));
        }
      },
    );
  }

  Widget _titleWrapper(BuildContext context, String text) {
    return Container(
      height: TITLE_HEIGHT,
      margin: EdgeInsets.fromLTRB(MARGIN_SIZE, 0, 0, 0),
      child: Text(
        dynamicEntity.title,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget _viewCountWrapper(String text) {
    return Container(
      margin: EdgeInsets.fromLTRB(MARGIN_SIZE, 0, 0, 0),
      height: ITEM_HEIGHT - TITLE_HEIGHT,
      child: Row(children: [
        Icon(
          Icons.remove_red_eye_outlined,
          size: 14.0,
          color: Colors.grey,
        ),
        SizedBox(width: 5),
        Text(
          dynamicEntity.viewCount.toString(),
          style: TextStyle(color: Colors.grey, fontSize: 14.0),
        ),
      ]),
    );
  }

  Widget _imageWrapper(String imageUrl) {
    return SizedBox(
      width: 150,
      height: ITEM_HEIGHT,
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            LinearProgressIndicator(value: downloadProgress.progress),
        errorWidget: (context, url, error) =>
            Image.asset('images/image-failed.png'),
      ),
    );
  }
}
