import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:home_framework/routers/fluro_router.dart';
import 'animation/custom_transition.dart';
import 'components/button_util.dart';

class CategoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('转场动画', style: Theme.of(context).textTheme.headline4),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ButtonUtil.primaryTextButton(
              '↓上移入',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.transitionPath,
                    transition: TransitionType.inFromTop);
              },
              context,
              height: 50,
              width: 150,
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ButtonUtil.primaryTextButton(
                  '→左移入',
                  () {
                    RouterManager.router.navigateTo(
                        context, RouterManager.transitionPath,
                        transition: TransitionType.inFromLeft);
                  },
                  context,
                  height: 50,
                  width: 150,
                ),
                Container(
                  width: 50,
                  height: 50,
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(25),
                    ),
                    color: Colors.blue[300],
                  ),
                ),
                ButtonUtil.primaryTextButton(
                  '←右移入',
                  () {
                    RouterManager.router.navigateTo(
                        context, RouterManager.transitionPath,
                        transition: TransitionType.inFromRight);
                  },
                  context,
                  height: 50,
                  width: 150,
                ),
              ],
            ),
            ButtonUtil.primaryTextButton(
              '↑下移入',
              () {
                RouterManager.router.navigateTo(
                  context,
                  RouterManager.transitionPath,
                  transition: TransitionType.inFromBottom,
                  transitionDuration: Duration(milliseconds: 1000),
                );
              },
              context,
              height: 50,
              width: 150,
            ),
            _getOtherPredefinedTransitionButtons(context),
            _getCustomTransitionButtons(context),
          ],
        ),
      ),
    );
  }

  Widget _getOtherPredefinedTransitionButtons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ButtonUtil.primaryTextButton(
          '预定义',
          () {
            RouterManager.router.navigateTo(
              context,
              RouterManager.transitionPath,
            );
          },
          context,
          height: 50,
          width: 80,
        ),
        ButtonUtil.primaryTextButton(
          '渐现',
          () {
            RouterManager.router.navigateTo(
                context, RouterManager.transitionPath,
                transition: TransitionType.fadeIn,
                transitionDuration: Duration(milliseconds: 1000));
          },
          context,
          height: 50,
          width: 80,
        ),
        ButtonUtil.primaryTextButton(
          'iOS 全屏',
          () {
            RouterManager.router.navigateTo(
              context,
              RouterManager.transitionPath,
              transition: TransitionType.cupertinoFullScreenDialog,
            );
          },
          context,
          height: 50,
          width: 80,
        ),
        ButtonUtil.primaryTextButton(
          '安卓 全屏',
          () {
            RouterManager.router.navigateTo(
              context,
              RouterManager.transitionPath,
              transition: TransitionType.materialFullScreenDialog,
            );
          },
          context,
          height: 50,
          width: 80,
        ),
      ],
    );
  }

  Widget _getCustomTransitionButtons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ButtonUtil.primaryTextButton(
          '逆时针',
          () {
            RouterManager.router.navigateTo(
              context,
              RouterManager.transitionPath,
              transition: TransitionType.custom,
              transitionBuilder:
                  (context, animation, secondaryAnimation, child) {
                return RotationTransition(
                  turns: Tween<double>(
                    begin: 0.25,
                    end: 0.0,
                  ).animate(animation),
                  child: child,
                );
              },
            );
          },
          context,
          height: 50,
          width: 80,
        ),
        ButtonUtil.primaryTextButton(
          '顺时针',
          () {
            RouterManager.router.navigateTo(
              context,
              RouterManager.transitionPath,
              transition: TransitionType.custom,
              transitionBuilder:
                  (context, animation, secondaryAnimation, child) {
                return RotationTransition(
                  alignment: Alignment.bottomLeft,
                  turns: Tween<double>(
                    begin: -0.25,
                    end: 0.0,
                  ).animate(animation),
                  child: child,
                );
              },
            );
          },
          context,
          height: 50,
          width: 80,
        ),
        ButtonUtil.primaryTextButton(
          '变形',
          () {
            RouterManager.router.navigateTo(
              context,
              RouterManager.transitionPath,
              transition: TransitionType.custom,
              transitionBuilder:
                  (context, animation, secondaryAnimation, child) {
                return SkewTransition(
                  turns: Tween<double>(
                    begin: -0.05,
                    end: 0.0,
                  ).animate(animation),
                  child: child,
                );
              },
            );
          },
          context,
          height: 50,
          width: 80,
        ),
        ButtonUtil.primaryTextButton(
          '缩放',
          () {
            RouterManager.router.navigateTo(
              context,
              RouterManager.transitionPath,
              transition: TransitionType.custom,
              transitionBuilder:
                  (context, animation, secondaryAnimation, child) {
                return ScaleTransition(
                  scale: Tween<double>(
                    begin: 0.5,
                    end: 1.0,
                  ).animate(animation),
                  child: child,
                );
              },
            );
          },
          context,
          height: 50,
          width: 80,
        ),
      ],
    );
  }
}
