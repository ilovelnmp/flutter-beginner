import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:home_framework/routers/fluro_router.dart';
import '../../components/button_util.dart';

class CategoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('状态管理演示'),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: ListView(
          children: [
            SizedBox(
              height: 15,
            ),
            ButtonUtil.primaryTextButton(
              '使用 ModelBinding',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.stateDemoPath,
                    transition: TransitionType.inFromTop);
              },
              context,
              height: 50,
              width: 200,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              '使用 setState',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.stateDemoPath1,
                    transition: TransitionType.native);
              },
              context,
              height: 50,
              width: 200,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              '状态管理-购物车',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.goodsPath,
                    transition: TransitionType.native);
              },
              context,
              height: 50,
              width: 200,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              '模型绑定版本0',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.modelV0Path,
                    transition: TransitionType.native);
              },
              context,
              height: 50,
              width: 200,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              '模型绑定版本1',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.modelV1Path,
                    transition: TransitionType.native);
              },
              context,
              height: 50,
              width: 200,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              '模型绑定泛型版',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.modelV2Path,
                    transition: TransitionType.native);
              },
              context,
              height: 50,
              width: 200,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              'Future 状态管理',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.personnalHomePath,
                    transition: TransitionType.inFromRight);
              },
              context,
              height: 50,
              width: 200,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              'Stream 状态管理',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.streamPath,
                    transition: TransitionType.inFromRight);
              },
              context,
              height: 50,
              width: 200,
            ),
          ],
        ),
      ),
    );
  }
}
