import 'dart:async';

import 'package:flutter/material.dart';
import 'package:home_framework/routers/fluro_router.dart';

class Splash extends StatefulWidget {
  Splash({Key key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  bool _initialized = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 120,
          height: 120,
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(120 / 2)),
          ),
          child: Image.asset(
            'images/logo.png',
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!_initialized) {
      _initialized = true;
      Timer(const Duration(milliseconds: 2000), () {
        RouterManager.router
            .navigateTo(context, RouterManager.homePath, clearStack: true);
        //Navigator.of(context).pushReplacementNamed(RouterTable.homePath);
      });
    }
  }
}
