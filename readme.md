本源码是在掘金专栏：[Flutter 入门与实战](https://juejin.cn/column/6960631670378594311)的示例代码，代码并未按章节分开，可以对照专栏文章阅读本源码。欢迎大家前往掘金点赞支持，或在评论区交流。也欢迎关注本人公众号：岛上码农。

![qrcode_island_coder.jpg](https://gitee.com/island-coder/flutter-beginner/raw/master/qrcode_island_coder.jpg)
