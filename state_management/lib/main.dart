import 'package:flutter/material.dart';
import 'states/cart_model.dart';
import 'package:provider/provider.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:home_framework/utils/cookie_manager.dart';
import 'routers/fluro_router.dart';
import 'view_models/dynamic/dynamic_model.dart';

void main() {
  //WidgetsFlutterBinding.ensureInitialized();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => CartModel()),
      ChangeNotifierProvider(create: (context) => DynamicModel()),
    ],
    child: MyApp(),
  ));
  CookieManager.instance.initCookie();
}

class MyApp extends StatelessWidget {
  //final GlobalKey navigationKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    RouterManager.initRouter(whiteList: [
      '/',
      '/login',
      '/home',
      '/dynamic',
      '/transition',
      '/dynamic/:id',
      '/dynamic/edit/:id',
      '/dynamic/add'
    ]);
    return MaterialApp(
      title: 'App 框架',
      theme: ThemeData(
        primaryColor: Colors.blue,
        accentColor: Colors.blue[600],
        textTheme: TextTheme(
          headline1: TextStyle(
              fontSize: 36.0, fontWeight: FontWeight.bold, color: Colors.white),
          headline2: TextStyle(
              fontSize: 32.0, fontWeight: FontWeight.w400, color: Colors.white),
          headline3: TextStyle(
              fontSize: 28.0, fontWeight: FontWeight.w400, color: Colors.white),
          headline4: TextStyle(
              fontSize: 24.0, fontWeight: FontWeight.w400, color: Colors.white),
          headline6: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w200,
            color: Colors.black,
          ),
          bodyText1: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.w200,
          ),
        ),
        // fontFamily: 'Tahoma',
      ),
      //navigatorKey: navigationKey,
      onGenerateRoute:
          RouterManager.router.generator, //RouterTable.onGenerateRoute,
      builder: EasyLoading.init(),
    );
  }
}
