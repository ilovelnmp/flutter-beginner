本源码是在掘金专栏：[Flutter 入门与实战](https://juejin.cn/column/6960631670378594311)的状态管理篇示例代码。欢迎大家前往掘金点赞支持（掘金账号：岛上码农），或在评论区交流。也欢迎关注本人公众号：岛上码农。

本代码已经支持 Dart 2.12 版本后的`null safety`特性。

**注意：网络代码部分对于安卓模拟器来说无法识别 localhost 或 127.0.0.1，因此需要将本地调试地址改为本机 IP：192.168.x.x**

![qrcode_island_coder.jpg](https://gitee.com/island-coder/flutter-beginner/raw/master/qrcode_island_coder.jpg)
