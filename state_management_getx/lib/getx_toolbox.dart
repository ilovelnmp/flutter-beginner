import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

// ignore: non_constant_identifier_names
void getxToolBox() {
  Get.to(() => Home());
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String? content;
    try {
      CacheData cache = Get.find();
      content = cache.name;
    } catch (e, s) {
      print(e.toString());
    }
    return Scaffold(
      appBar: AppBar(title: Text('Home')),
      body: Center(
        child: Text(content ?? 'Home'),
      ),
    );
  }
}

class GetXDemo extends StatelessWidget {
  const GetXDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('GetX 工具箱')),
      body: ListView(
        children: [
          TextButton(
            onPressed: () {
              Get.to(() => Home());
            },
            child: Text('路由'),
          ),
          TextButton(
            onPressed: () {
              Get.snackbar('SnackBar', '这是GetX的SnackBar');
            },
            child: Text('Snack'),
          ),
          TextButton(
            onPressed: () {
              Get.defaultDialog(
                title: '对话框',
                content: Text('对话框内容'),
                onConfirm: () {
                  print('Confirm');
                  Get.back();
                },
                onCancel: () {
                  print('Cancel');
                },
              );
            },
            child: Text('对话框'),
          ),
          TextButton(
            onPressed: () {
              Get.put(CacheData(name: '这是缓存数据'));
            },
            child: Text('缓存共享对象'),
          ),
          TextButton(
            onPressed: () {
              GetStorage storage = GetStorage();
              storage.write('name', '岛上码农');
            },
            child: Text('离线存储'),
          ),
          TextButton(
            onPressed: () {
              GetStorage storage = GetStorage();
              Get.snackbar('读取结果', storage.read('name'));
            },
            child: Text('读取离线数据'),
          ),
          TextButton(
            onPressed: () {
              Get.changeTheme(
                  Get.isDarkMode ? ThemeData.light() : ThemeData.dark());
            },
            child: Text('更改主题'),
          ),
          TextButton(
            onPressed: () {
              var locale = Locale('en', 'US');
              Get.updateLocale(locale);
            },
            child: Text('name'.tr),
          ),
        ],
      ),
    );
  }
}

class CacheData {
  final String name;
  CacheData({required this.name});
}

class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          'name': 'Island Coder',
        },
        'zh_CN': {
          'name': '岛上码农',
        }
      };
}
