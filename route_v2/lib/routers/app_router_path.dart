import 'package:flutter/cupertino.dart';

enum RouterPaths { splash, dynamicList, dynamicDetail, notFound }

class AppRouterConfiguration {
  final RouterPaths path;
  final dynamic state;
  AppRouterConfiguration(this.path, this.state);
}

class AppRouterInformationParser
    extends RouteInformationParser<AppRouterConfiguration> {
  @override
  Future<AppRouterConfiguration> parseRouteInformation(
      RouteInformation routeInformation) async {
    final String routeName = routeInformation.location;
    switch (routeName) {
      case '/':
        return AppRouterConfiguration(
            RouterPaths.splash, routeInformation.state);
      case '/home':
        return AppRouterConfiguration(
            RouterPaths.dynamicList, routeInformation.state);
      case '/dynamicDetail':
        return AppRouterConfiguration(
            RouterPaths.dynamicDetail, routeInformation.state);
      default:
        return AppRouterConfiguration(
            RouterPaths.notFound, routeInformation.state);
    }
  }

  @override
  RouteInformation restoreRouteInformation(
      AppRouterConfiguration configuration) {
    switch (configuration.path) {
      case RouterPaths.splash:
        return RouteInformation(location: '/', state: configuration.state);
      case RouterPaths.dynamicList:
        return RouteInformation(location: '/home', state: configuration.state);
      case RouterPaths.dynamicDetail:
        return RouteInformation(
            location: '/dynamicDetail', state: configuration.state);
      default:
        return RouteInformation(location: '/404', state: configuration.state);
    }
  }
}
