import 'package:flutter/material.dart';
import 'package:home_framework/pages/state_demo/leisi.dart';
import 'package:home_framework/pages/state_demo/stateless_no_depend.dart';
import 'package:home_framework/pages/state_demo/xiaofu_3.dart';
import 'package:home_framework/states/face_emotion.dart';
import 'package:home_framework/states/model_binding_v2.dart';

class StatefulStatelessDemoPage extends StatelessWidget {
  StatefulStatelessDemoPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('3年后'),
      ),
      body: ModelBindingV2(
        child: Column(
          children: [
            Xiaofu3(),
            Leisi(),
            StatelessNoDepend(),
          ],
        ),
        create: () => FaceEmotion(emotion: '惊讶'),
      ),
    );
  }
}
