import 'package:flutter/material.dart';
import 'dynamic.dart';
import 'message.dart';
import 'category.dart';
import 'mine_sliver.dart';

class AppHomePage extends StatefulWidget {
  AppHomePage({Key key}) : super(key: key);

  @override
  _AppHomePageState createState() => _AppHomePageState();
}

class _AppHomePageState extends State<AppHomePage> {
  int _index = 0;

  List<Widget> _homeWidgets = [
    DynamicPage(),
    MessagePage(),
    CategoryPage(),
    MineSliverPage(),
  ];

  void _onBottomNagigationBarTapped(index) {
    setState(() {
      _index = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: IndexedStack(
          index: _index,
          children: _homeWidgets,
        ),
        decoration: BoxDecoration(color: Colors.grey[100]),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _index,
        onTap: _onBottomNagigationBarTapped,
        selectedItemColor: Theme.of(context).primaryColor,
        items: [
          _getBottomNavItem(
              '动态', 'images/dynamic.png', 'images/dynamic-hover.png'),
          _getBottomNavItem(
              ' 消息', 'images/message.png', 'images/message-hover.png'),
          _getBottomNavItem(
              '分类浏览', 'images/category.png', 'images/category-hover.png'),
          _getBottomNavItem('个人中心', 'images/mine.png', 'images/mine-hover.png'),
        ],
      ),
    );
  }

  BottomNavigationBarItem _getBottomNavItem(
      String title, String normalIcon, String activeIcon) {
    return BottomNavigationBarItem(
      icon: Image.asset(
        normalIcon,
        width: 32,
        height: 28,
      ),
      activeIcon: Image.asset(
        activeIcon,
        width: 32,
        height: 28,
      ),
      label: title,
    );
  }
}
