import 'package:flutter/material.dart';
import 'package:home_framework/models/goods_entity.dart';
import 'package:home_framework/pages/state_demo/goods_item.dart';
import '../../states/cart_model.dart';
import 'package:provider/provider.dart';

class MyCart extends StatelessWidget {
  const MyCart({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<GoodsEntity> goods = context.read<CartModel>().items;
    return Scaffold(
      appBar: AppBar(
        title: Text('购物车'),
      ),
      body: Container(
        child: Consumer<CartModel>(
          builder: (context, cart, child) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return GoodsItem(goods[index]);
              },
              itemCount: goods.length,
            );
          },
        ),
        color: Colors.grey[50],
      ),
      bottomNavigationBar: Container(
        height: 70,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
              top: BorderSide(
            color: Colors.grey[400],
            width: 1.0,
          )),
          color: Colors.white,
        ),
        child: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: Text('总计：￥${context.read<CartModel>().totalPrice}'),
                  ),
                  TextButton(onPressed: () {}, child: Text('结算')),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
