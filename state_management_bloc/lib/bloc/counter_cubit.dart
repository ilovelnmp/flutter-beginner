import 'package:bloc/bloc.dart';

class CounterCubit extends Cubit<int> {
  CounterCubit({initial = 0}) : super(initial);

  void increment() => emit(state + 1);
  void decrement() => emit(state - 1);

  @override
  void onChange(Change<int> change) {
    super.onChange(change);
  }
}
