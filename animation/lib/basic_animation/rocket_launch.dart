import 'package:flutter/material.dart';

class RocketLaunch extends StatefulWidget {
  RocketLaunch({Key? key}) : super(key: key);

  @override
  _RocketLaunchState createState() => _RocketLaunchState();
}

class _RocketLaunchState extends State<RocketLaunch> {
  var rocketBottom = -80.0;
  var rocketWidth = 160.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('火箭发射'),
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Image.asset(
              'images/earth.jpeg',
              height: double.infinity,
              fit: BoxFit.fill,
            ),
            AnimatedPositioned(
              child: Image.asset(
                'images/rocket.png',
                fit: BoxFit.fitWidth,
              ),
              bottom: rocketBottom,
              width: rocketWidth,
              duration: Duration(seconds: 5),
              curve: Curves.easeInCubic,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text(
          '发射',
          style: TextStyle(
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
        onPressed: () {
          setState(() {
            rocketBottom = MediaQuery.of(context).size.height;
            rocketWidth = 40.0;
          });
        },
      ),
    );
  }
}
