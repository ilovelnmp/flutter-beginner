import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:home_framework/models/dynamic_entity.dart';
import 'dynamic_item.dart';
import 'dynamic_mock_data.dart';

class DynamicPage extends StatefulWidget {
  DynamicPage({Key key}) : super(key: key);

  @override
  _DynamicPageState createState() => _DynamicPageState();
}

class _DynamicPageState extends State<DynamicPage> {
  List<DynamicEntity> _listItems = [];
  int _currentPage = 1;
  static const int PAGE_SIZE = 20;

  EasyRefreshController _refreshController = EasyRefreshController();

  void _refresh() async {
    _currentPage = 1;
    _requestNewItems();
  }

  void _load() async {
    _currentPage += 1;
    _requestNewItems();
  }

  @override
  void initState() {
    super.initState();
    Timer(const Duration(milliseconds: 500), () {
      _refreshController.callRefresh();
    });
  }

  void _requestNewItems() async {
    List<Map<String, dynamic>> _jsonItems =
        await DynamicMockData.list(_currentPage, PAGE_SIZE);
    List<DynamicEntity> _newItems =
        _jsonItems.map((json) => DynamicEntity.fromJson(json)).toList();
    this.setState(() {
      if (_currentPage > 1) {
        _listItems += _newItems;
      } else {
        _listItems = _newItems;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('动态', style: Theme.of(context).textTheme.headline4),
        brightness: Brightness.dark,
      ),
      body: EasyRefresh(
        controller: _refreshController,
        onRefresh: () async {
          _refresh();
        },
        onLoad: () async {
          _load();
        },
        child: ListView.builder(
            itemCount: _listItems.length,
            itemBuilder: (context, index) {
              return DynamicItem(_listItems[index]);
            }),
      ),
    );
  }
}
