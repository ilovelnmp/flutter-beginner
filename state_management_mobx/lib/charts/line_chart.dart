import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'chart_store.dart';

class LineChartDemo extends StatelessWidget {
  LineChartDemo({Key? key}) : super(key: key);

  final ChartStore store = ChartStore();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('曲线')),
      body: Observer(
        builder: (context) => LineChart(
          sampleData1(store.lineYData),
          swapAnimationDuration: const Duration(milliseconds: 250),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          store.featchLineData();
        },
        child: Icon(Icons.refresh, color: Colors.white),
      ),
    );
  }

  LineChartData sampleData1(List<double> yData) => LineChartData(
        lineTouchData: lineTouchData1,
        gridData: gridData,
        titlesData: titlesData1,
        borderData: borderData,
        lineBarsData: lineBarsData1(yData),
        minX: 0,
        maxX: 15,
        maxY: yData
                .reduce((value, element) => value < element ? element : value)
                .toDouble() +
            1.0,
        minY: yData
                .reduce((value, element) => value > element ? element : value)
                .toDouble() -
            1.0,
      );

  // 触摸显示数据
  LineTouchData get lineTouchData1 => LineTouchData(
        handleBuiltInTouches: true,
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.blue.withOpacity(0.5),
        ),
      );

  List<LineChartBarData> lineBarsData1(List<double> yData) {
    double x = 1;
    return [
      LineChartBarData(
        //曲线还是折线
        isCurved: true,
        //线条颜色
        colors: [const Color(0xff4a99fa)],
        //线粗细
        barWidth: 2,
        //是否圆形笔头
        isStrokeCapRound: true,
        //是否显示点数据
        dotData: FlDotData(show: true),
        //图形覆盖区域是否显示
        belowBarData: BarAreaData(show: true),
        //坐标（x,y） 点集合
        spots: yData.map((value) {
          FlSpot spot = FlSpot(x, value);
          x += 2;
          return spot;
        }).toList(),
      ),
    ];
  }

  // 标题显示：包括底部标题栏，右侧标题和顶部标题以及左侧标题栏
  FlTitlesData get titlesData1 => FlTitlesData(
        bottomTitles: bottomTitles,
        rightTitles: SideTitles(showTitles: false),
        topTitles: SideTitles(showTitles: false),
        leftTitles: leftTitles(
          getTitles: (value) {
            if (value == 24) return '24';
            if (value == 26) return '26';
            if (value == 28) return '28';
            if (value == 30) return '30';
            if (value == 32) return '32';
            if (value == 34) return '34';
            if (value == 36) return '36';
            return '';
          },
        ),
      );

  SideTitles get bottomTitles => SideTitles(
        showTitles: true,
        reservedSize: 40,
        margin: 8,
        interval: 1,
        getTextStyles: (context, value) => const TextStyle(
          color: Colors.blue,
          fontSize: 16,
        ),
        getTitles: (value) {
          switch (value.toInt()) {
            case 1:
              return '1';
            case 5:
              return '5';
            case 9:
              return '9';
            case 13:
              return '13';
          }
          return '';
        },
      );

  // 网格
  FlGridData get gridData => FlGridData(show: true);

  // 边框数据
  FlBorderData get borderData => FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(color: Color(0xff4e4965), width: 2),
          left: BorderSide(color: Colors.transparent),
          right: BorderSide(color: Colors.transparent),
          top: BorderSide(color: Colors.transparent),
        ),
      );

  SideTitles leftTitles({required GetTitleFunction getTitles}) => SideTitles(
        getTitles: getTitles,
        showTitles: true,
        margin: 10,
        interval: 2,
        reservedSize: 60,
        getTextStyles: (context, value) => const TextStyle(
          color: Color(0xff75729e),
          fontWeight: FontWeight.bold,
          fontSize: 14,
        ),
      );
}
