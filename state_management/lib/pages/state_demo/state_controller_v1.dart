import 'package:flutter/material.dart';
import 'package:home_framework/states/model_binding_v1.dart';

class StateViewControllerV1 extends StatelessWidget {
  StateViewControllerV1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('模型绑定版本1'),
      ),
      body: Center(
        child: ModelBindingV1(
          child: ViewController(),
        ),
      ),
    );
  }
}

class ViewController extends StatelessWidget {
  const ViewController({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Text('Hello World ${ViewModel.of(context).value}'),
      onPressed: () {
        ViewModel model = ViewModel.of(context);
        ViewModel.update(context, ViewModel(value: model.value + 1));
      },
    );
  }
}
