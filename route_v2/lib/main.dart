import 'package:flutter/material.dart';
import 'package:home_framework/routers/app_router_path.dart';
import 'package:home_framework/routers/router_delegate.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: '2.0路由',
      routerDelegate: AppRouterDelegate(),
      routeInformationParser: AppRouterInformationParser(),
      theme: ThemeData(
        primaryColor: Colors.blue,
        accentColor: Colors.blue[600],
        textTheme: TextTheme(
          headline4: TextStyle(
              fontSize: 24.0, fontWeight: FontWeight.w400, color: Colors.white),
          headline6: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w200,
            color: Colors.black,
          ),
          bodyText1: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.w200,
          ),
        ),
        fontFamily: 'Georgia',
      ),
    );
  }
}
