import 'package:flutter/material.dart';

class Xiaofu extends StatefulWidget {
  Xiaofu({Key? key}) : super(key: key) {
    print('constructor: 小芙');
  }

  @override
  _XiaofuState createState() => _XiaofuState();
}

class _XiaofuState extends State<Xiaofu> {
  String _face = '平静';

  @override
  void initState() {
    super.initState();
    print('initState：小芙花了2小时化妆');
  }

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    print('didUpdateWidget：小芙出现了');
  }

  @override
  void reassemble() {
    super.reassemble();
    print('reassemble：小芙');
  }

  @override
  Widget build(BuildContext context) {
    print('build：小芙');
    return Center(
      child: Column(children: [
        Text('表情：$_face'),
        TextButton(
            onPressed: () {
              setState(() {
                _face = '失望';
                print('小芙的表情变了');
              });
            },
            child: Text('小芙表情变了')),
      ]),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print('didChangeDependencies：小芙');
  }

  @override
  void deactivate() {
    print('deactivate：小芙的情绪恢复了');
    super.deactivate();
  }

  @override
  void dispose() {
    print('dispose：小芙将雷思从关注列表移除了');
    super.dispose();
  }
}
