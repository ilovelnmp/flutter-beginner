import 'dart:ui';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MoonStep1Controller extends GetxController {
  final double seaLevel = Get.height - 180.0;
  final double finalPosition = 300;
  late double _mooenCenterY;
  get mooenCenterY => _mooenCenterY;
  late Timer _downcountTimer;

  @override
  void onInit() {
    _mooenCenterY = seaLevel;
    super.onInit();
  }

  @override
  void onReady() {
    _downcountTimer = Timer.periodic(Duration(milliseconds: 40), repaint);
    super.onReady();
  }

  void repaint(Timer timer) {
    bool needUpdate = false;
    if (_mooenCenterY > finalPosition) {
      _mooenCenterY -= 1;
      needUpdate = true;
    } else {
      timer.cancel();
    }

    if (needUpdate) {
      update();
    }
  }

  @override
  void onClose() {
    _downcountTimer.cancel();
    super.onClose();
  }
}

class DrawMooenPage extends StatelessWidget {
  DrawMooenPage({Key? key}) : super(key: key);
  final MoonStep1Controller controller = MoonStep1Controller();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MoonStep1Controller>(
      init: controller,
      builder: (store) => CustomPaint(
        child: null,
        foregroundPainter: MoonStep1Painter(
          mooenCenterY: store.mooenCenterY,
          seaLevel: store.seaLevel,
        ),
      ),
    );
  }
}

class MoonStep1Painter extends CustomPainter {
  final double mooenCenterY;
  final double seaLevel;
  MoonStep1Painter({
    required this.mooenCenterY,
    required this.seaLevel,
  }) : super();

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawColor(Color(0xFF252525), BlendMode.color);
    var center = size / 2;
    paintMooen(canvas, Offset(center.width, mooenCenterY), 90);
    paintSea(canvas, size);
    paintPoet(canvas, '海上升明月 天涯共此时', size);
  }

  void paintMooen(Canvas canvas, Offset center, double raidus) {
    var mooenPaint = Paint()..color = Colors.yellow[100]!;
    mooenPaint.strokeWidth = 2.0;
    canvas.drawCircle(
      center,
      raidus,
      mooenPaint,
    );

    var lightPaint = Paint()..color = Colors.yellow[100]!.withAlpha(30);
    lightPaint.strokeWidth = 2.0;
    canvas.drawCircle(
      center,
      raidus + 3,
      lightPaint,
    );
  }

  void paintSea(Canvas canvas, Size size) {
    int seaColor = 0xFF020408;
    var seaPaint = Paint()..color = Color(seaColor);
    seaPaint.strokeWidth = 2.0;

    Path seaPath = Path();
    seaPath.moveTo(0, seaLevel);

    seaPath.lineTo(size.width, seaLevel);
    seaPath.lineTo(size.width, size.height);
    seaPath.lineTo(0, size.height);
    seaPath.lineTo(0, seaLevel);
    canvas.drawPath(seaPath, seaPaint);
  }

  void paintPoet(Canvas canvas, String poet, Size size) {
    var style = TextStyle(
      fontWeight: FontWeight.w300,
      fontSize: 26.0,
      color: Colors.yellow[100],
    );

    final ParagraphBuilder paragraphBuilder = ParagraphBuilder(
      ParagraphStyle(
        fontSize: style.fontSize,
        fontFamily: style.fontFamily,
        fontStyle: style.fontStyle,
        fontWeight: style.fontWeight,
        textAlign: TextAlign.center,
      ),
    )
      ..pushStyle(style.getTextStyle())
      ..addText(poet);
    final Paragraph paragraph = paragraphBuilder.build()
      ..layout(ParagraphConstraints(width: size.width));
    canvas.drawParagraph(paragraph, Offset(0, 100));
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
