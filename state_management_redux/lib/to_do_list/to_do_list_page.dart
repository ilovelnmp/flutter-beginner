import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:meta/meta.dart';
import 'package:redux/redux.dart';
import 'actions.dart';
import 'state.dart';
import 'to_do_item.dart';

class ToDoListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) {
        return Scaffold(
          appBar: AppBar(
            title: Text(viewModel.pageTitle),
          ),
          body: ListView(
              children: viewModel.items
                  .map((_ItemViewModel item) => _createWidget(item))
                  .toList()),
          floatingActionButton: FloatingActionButton(
            onPressed: viewModel.onAddItem,
            tooltip: viewModel.newItemToolTip,
            child: Icon(viewModel.newItemIcon),
          ),
        );
      });

  Widget _createWidget(_ItemViewModel item) {
    if (item.viewState == ItemViewState.add) {
      return _createEmptyItemWidget(item);
    } else {
      return _createToDoItemWidget(item);
    }
  }

  Widget _createEmptyItemWidget(_ItemViewModel item) => Column(
        children: [
          TextField(
            onSubmitted: item.onIconButtonPressed,
            autofocus: true,
            decoration: InputDecoration(
              hintText: item.title,
            ),
          )
        ],
      );

  Widget _createToDoItemWidget(_ItemViewModel item) => Row(
        children: [
          Text(item.title),
          TextButton(
            onPressed: () {
              item.onIconButtonPressed(null);
            },
            child: Icon(
              item.iconData,
              semanticLabel: item.toolTip,
            ),
          )
        ],
      );
}

class _ViewModel {
  final String pageTitle;
  final List<_ItemViewModel> items;
  final Function() onAddItem;
  final String newItemToolTip;
  final IconData newItemIcon;

  _ViewModel(this.pageTitle, this.items, this.onAddItem, this.newItemToolTip,
      this.newItemIcon);

  factory _ViewModel.create(Store<AppState> store) {
    List<_ItemViewModel> items = store.state.toDos.map((ToDoItem item) {
      return _ItemViewModel(item.title, (title) {
        store.dispatch(RemoveItemAction(item));
        store.dispatch(SaveListAction());
      }, 'Delete', Icons.delete, ItemViewState.delete);
    }).toList();

    if (store.state.listState == ListState.listWithNewItem) {
      //   ToDoItem item = ToDoItem('new');
      //   items.add(_ToDoItemViewModel('new', () {
      //     store.dispatch(RemoveItemAction(item));
      //     store.dispatch(SaveListAction());
      //   }, 'Delete', Icons.delete));

      items.add(_ItemViewModel('Type the next task here', (title) {
        //store.dispatch(DisplayListWithNewItemAction());
        store.dispatch(AddItemAction(ToDoItem(title!)));
        store.dispatch(SaveListAction());
      }, 'Add', Icons.add, ItemViewState.add));
    }

    return _ViewModel('To Do', items, () {
      store.dispatch(store.state.listState == ListState.listWithNewItem
          ? DisplayListOnlyAction()
          : DisplayListWithNewItemAction());
    }, 'Add new to-do item', Icons.add);
  }
}

@immutable
class _ItemViewModel {
  final String title;
  final ValueChanged<String?> onIconButtonPressed;
  final String toolTip;
  final IconData iconData;
  final ItemViewState viewState;

  _ItemViewModel(this.title, this.onIconButtonPressed, this.toolTip,
      this.iconData, this.viewState);
}

enum ItemViewState {
  add,
  delete,
}
