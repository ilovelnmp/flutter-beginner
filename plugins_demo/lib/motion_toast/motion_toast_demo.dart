import 'package:flutter/material.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import '../components/primary_button.dart';

class MotionToastDemo extends StatelessWidget {
  const MotionToastDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Motion Toast'),
      ),
      body: ListView(
        children: [
          PrimaryButton(
            onPressed: () {
              MotionToast.success(description: '操作成功！').show(context);
            },
            title: '成功提示',
          ),
          PrimaryButton(
            onPressed: () {
              MotionToast.error(
                description: '发生错误！',
                width: 300,
                position: MOTION_TOAST_POSITION.center,
              ).show(context);
            },
            title: '错误提示',
          ),
          PrimaryButton(
            onPressed: () {
              MotionToast.info(
                description: '这是一条提醒，可能会有很多行。toast 会自动调整高度显示',
                title: '提醒',
                titleStyle: TextStyle(fontWeight: FontWeight.bold),
                position: MOTION_TOAST_POSITION.bottom,
                animationType: ANIMATION.fromBottom,
                animationCurve: Curves.linear,
                dismissable: true,
              ).show(context);
            },
            title: '信息提醒',
          ),
          PrimaryButton(
            onPressed: () {
              MotionToast.delete(
                description: '已成功删除',
                position: MOTION_TOAST_POSITION.bottom,
                animationType: ANIMATION.fromLeft,
                animationCurve: Curves.bounceIn,
              ).show(context);
            },
            title: '删除提醒',
          ),
          PrimaryButton(
            onPressed: () {
              MotionToast(
                description: '这是自定义 toast',
                icon: Icons.flag,
                primaryColor: Colors.blue,
                secondaryColor: Colors.green[300],
                descriptionStyle: TextStyle(
                  color: Colors.white,
                ),
                position: MOTION_TOAST_POSITION.center,
                animationType: ANIMATION.fromRight,
                animationCurve: Curves.easeIn,
              ).show(context);
            },
            title: '自定义 Toast',
          ),
        ],
      ),
    );
  }
}
