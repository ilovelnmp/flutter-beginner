import 'dart:ui';
import 'package:flutter/material.dart';

class BasicPaintPage extends StatelessWidget {
  const BasicPaintPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      child: Center(
        child: ClipOval(
          child: Image.asset(
            'images/beauty.jpeg',
            width: 200,
            height: 200,
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
      painter: BackgroundPainter(),
      foregroundPainter: ForegroundPainter(),
    );
  }
}

class BackgroundPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawColor(Color(0xFFF1F1F1), BlendMode.color);
    var center = size / 2;
    var paint = Paint()..color = Color(0xFF2080E5);
    paint.strokeWidth = 2.0;

    canvas.drawRect(
      Rect.fromLTWH(center.width - 120, center.height - 120, 240, 240),
      paint,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class ForegroundPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var center = size / 2;
    var paint = Paint()..color = Color(0x80F53010);
    paint.strokeWidth = 2.0;

    canvas.drawCircle(
      Offset(center.width, center.height),
      100,
      paint,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
