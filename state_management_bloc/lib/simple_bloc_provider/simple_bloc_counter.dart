import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';

import 'person.dart';
import 'simple_bloc_provider.dart';
import '../bloc/counter_cubit.dart';

class SimpleBlocCounterPage extends StatelessWidget {
  final counter = CounterCubit();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bloc 计数器'),
      ),
      body: Center(
        child: SimpleBlocProvider<int>(
          builder: (count) => Text(
            '$count',
            style: TextStyle(
              fontSize: 32,
              color: Colors.blue,
            ),
          ),
          bloc: counter,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          counter.increment();
        },
        tooltip: '点击增加',
        child: Icon(Icons.add),
      ),
    );
  }
}

abstract class PersonEvent {}

class UsingCnNameEvent extends PersonEvent {}

class UsingEnNameEvent extends PersonEvent {}

class PersonBloc extends Bloc<PersonEvent, Person> {
  PersonBloc(Person person) : super(person) {
    on<UsingCnNameEvent>(
        (event, emit) => emit(Person(name: '岛上码农', gender: '男')));
    on<UsingEnNameEvent>(
        (event, emit) => emit(Person(name: 'island-coder', gender: 'male')));
  }
}

class PersonBlocCounterPage extends StatelessWidget {
  final personBloc = PersonBloc(Person(name: '岛上码农', gender: '男'));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bloc 事件'),
      ),
      body: Center(
        child: SimpleBlocProvider<Person>(
          builder: (person) => Text(
            '姓名：${person.name}，性别：${person.gender}',
            style: TextStyle(
              fontSize: 22,
              color: Colors.blue,
            ),
          ),
          bloc: personBloc,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          personBloc.add(UsingEnNameEvent());
        },
        tooltip: '点击增加',
        child: Icon(Icons.add),
      ),
    );
  }
}
