import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class InkWellHeroAnimation extends StatelessWidget {
  const InkWellHeroAnimation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return Scaffold(
      appBar: AppBar(
        title: Text('Hero InkWell动画'),
        brightness: Brightness.dark,
      ),
      body: Container(
        child: Hero(
          createRectTween: (begin, end) {
            return RectTween(begin: begin, end: end);
          },
          child: InkWellImage(
            assetImageName: 'images/beauty.jpeg',
            imageSize: 50.0,
            borderRadius: 10.0,
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const InkWellHeroDetail(),
                ),
              );
            },
          ),
          tag: 'beauty',
        ),
      ),
    );
  }
}

class InkWellHeroDetail extends StatelessWidget {
  const InkWellHeroDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hero InkWell动画详情'),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: Hero(
          child: InkWellImage(
            assetImageName: 'images/beauty.jpeg',
            imageSize: 200.0,
            borderRadius: 100.0,
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
          tag: 'beauty',
        ),
      ),
    );
  }
}

class InkWellImage extends StatelessWidget {
  final assetImageName;
  final imageSize;
  final onTap;
  final borderRadius;
  const InkWellImage({
    Key? key,
    required this.assetImageName,
    required this.imageSize,
    required this.borderRadius,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blue.withOpacity(0.3),
      child: InkWell(
        child: Container(
          child: Image.asset(
            assetImageName,
            width: imageSize,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(borderRadius),
          ),
          clipBehavior: Clip.antiAlias,
        ),
        onTap: onTap,
      ),
    );
  }
}
