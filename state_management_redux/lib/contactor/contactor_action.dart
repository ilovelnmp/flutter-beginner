class RefreshAction {}

class LoadAction {}

class SuccessAction {
  final List<dynamic> jsonItems;
  final int currentPage;

  SuccessAction(this.jsonItems, this.currentPage);
}

class FailedAction {
  final String errorMessage;

  FailedAction(this.errorMessage);
}
