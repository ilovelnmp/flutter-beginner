import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../models/goods_entity.dart';

class GoodsItem extends StatelessWidget {
  final GoodsEntity goodsEntity;
  static const double ITEM_HEIGHT = 80;
  static const double TITLE_HEIGHT = 30;
  static const double MARGIN_SIZE = 10;
  const GoodsItem(this.goodsEntity, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.all(MARGIN_SIZE),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _imageWrapper(goodsEntity.imageUrl),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _titleWrapper(context, goodsEntity.name),
                _titleWrapper(context, '￥${goodsEntity.price}'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _titleWrapper(BuildContext context, String text) {
    return Container(
      height: TITLE_HEIGHT,
      margin: EdgeInsets.fromLTRB(MARGIN_SIZE, 0, 0, 0),
      alignment: Alignment.centerLeft,
      child: Text(
        text,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget _imageWrapper(String imageUrl) {
    return SizedBox(
      width: 80,
      height: ITEM_HEIGHT,
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            LinearProgressIndicator(value: downloadProgress.progress),
        errorWidget: (context, url, error) =>
            Image.asset('images/image-failed.png'),
      ),
    );
  }
}
